export const syncFetch = (link: string, name: string): any => {
    try {
        const response = fetch(`${link}/${name}`, {
            method: 'GET',
            headers: new Headers({
                'Accept': "application/json"
            })
        });
        return response
    }
    catch (error) {
        console.error(error);
    }
}
