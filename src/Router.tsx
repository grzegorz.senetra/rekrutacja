import * as React from "react";
import {
  Switch,
  Route,
} from "react-router-dom";
import { Router } from 'react-router';
import createHistory from 'history/createBrowserHistory';
import CountryProfile from "./components/CountryProfile";
import CountryList from "./components/CountryList";
import * as services from './services'
import { COUNTRIES_BY_NAME_LINK } from "./endpoints";
import { CountryNameAndCode } from "./types";

const history = createHistory();

function RouterComponent() {

    const [countries, setCountries] = React.useState<Array<CountryNameAndCode>>([])

    React.useEffect(() => {
        getCountries('united')
    }, [])
  
    const getCountries = (countryName: string) => {
        services.syncFetch(COUNTRIES_BY_NAME_LINK, countryName)
        .then((response: any) => response.json())
        .then((responseJson: any) => {

            let countries_tmp: Array<CountryNameAndCode> = []
            
            responseJson.map((item: any, index: number) => {
                countries_tmp.push({
                    code: item.alpha3Code,
                    name: item.name
                })
                return 0
            })
            setCountries(countries_tmp)
        })
        .catch((error: any) => alert(error))
    }

    const handleSearch = (countryName: string) => {
      getCountries(countryName)
    }

    return (
      <Router history={history}>
        <div>
          <Switch>
            <Route
              path="/:countryname"
              render={(props) => <CountryProfile {...props} />}
            />
            <Route
              path="/"
              render={() => (
                <CountryList
                  countries={countries}
                  handleSearch={(countryName: string) =>
                    handleSearch(countryName)
                  }
                />
              )}
            />
          </Switch>
        </div>
      </Router>
    );

}

export default RouterComponent