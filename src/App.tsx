import * as React from 'react'
import './App.css';
import RouterComponent from './Router';

function App() {

  return (
    <div>
      <h1>COUNTRY CURRENCY CHECKER</h1>
      <RouterComponent />
    </div>
  );
}

export default App;
