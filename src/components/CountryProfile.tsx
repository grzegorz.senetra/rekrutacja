import * as React from 'react'
import { Link } from 'react-router-dom'
import { Country, Currency } from '../types'
import * as services from '../services'
import { COUNTRY_BY_CODE } from '../endpoints'

interface IProps {
    location: any
}

export default function CountryProfile(props: IProps) {

    const [countryDetails, setCountryDetails] = React.useState<Country>({code: '', name: '', capital: '', currencies: []})

    const country: Country = props.location.state

    React.useEffect(() => {

        const getCountryDetails = (code: string) => {
            services.syncFetch(COUNTRY_BY_CODE, code)
            .then((response: any) => response.json())
            .then((responseJson: any) => {

                setCountryDetails({
                    code: responseJson.alpha3Code,
                    capital: responseJson.capital,
                    currencies: responseJson.currencies,
                    name: responseJson.name
                })
            })
            .catch((error: any) => alert(error))
        }

        getCountryDetails(country.code)
    }, [country])

    return (
      <div className="Profile">
        <div className="Card">
            <div className="Header">
                <Link className="Link" to={"/"}>← Wróć</Link>
                <p>{country.name}</p>
            </div>
            <div className="Divider"></div>
            <div className="Element">Capital: <span style={{fontSize: 20}}>{countryDetails.capital}</span></div><br />
            <div className="Element Currencies"><br />Currencies:<br />
                {countryDetails.currencies.map((currency: Currency, index: number) => (
                <p style={{marginLeft: 30}} key={index}>
                    - <span style={{color: 'green'}}>{currency.symbol} {currency.code}</span> {currency.name}<br />  
                </p>
                ))}
            </div>
        </div>
      </div>
    );
}