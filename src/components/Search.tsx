import * as React from 'react'

interface IProps {
    handleSearch: any
}

export default function Search(props: IProps) {

    const [searchText, setSearchText] = React.useState<string>("")

    const handleChange = (value: string) => {
        setSearchText(value)
    }

    return (
        <div className="Search">
            <label>
                Search: &nbsp;
                <input type="text" name="name" value={searchText} onChange={(e) => handleChange(e.target.value)} />
            </label>
            <button style={{cursor: 'pointer'}} onClick={() => props.handleSearch(searchText)}>submit</button>
        </div>
    )
}