import * as React from 'react'
import { CountryNameAndCode } from '../types'
import {
    Link
  } from "react-router-dom";
import Search from './Search';

interface IProps {
    countries: Array<CountryNameAndCode>,
    handleSearch: any
}

export default function CountryList(props: IProps) {

    const [countries, setCountries] = React.useState<Array<CountryNameAndCode> | null>()
    const [pagination, setPagination] = React.useState<number>(1)
    const [currentPage, setCurrentPage] = React.useState<number>(1)

    React.useEffect(() => {

        setPagination(parseInt((props.countries.length / 20 + 1).toString()))

        setCountries(props.countries)
    },[props.countries])

    const compare = (a: CountryNameAndCode, b: CountryNameAndCode): number => {

        if ( a.name < b.name ){
          return -1;
        }
        if ( a.name > b.name ){
          return 1;
        }
        return 0;
      }      

    const generateCountryList = (countries: Array<CountryNameAndCode>): JSX.Element => {

        countries.sort( compare )

        let JSX_List: any = []

        let i = currentPage * 20

        let countries_paginated = countries.slice(i - 20, i)

        countries_paginated.map((country: CountryNameAndCode, index: number = i - 20) => {
            JSX_List.push(
                <Link key={index} className="Linkclass" to={{pathname: `/${country.name}`, state: country }} >
                    <li>{country.name}</li>
                </Link>
            )
            return 0
        })

        return JSX_List;
    };

    const generatePagination = (pagination: number): JSX.Element => {
        
        let JSX_Pagination: any = []

        for (let i = 1; i <= pagination; i++) {
            JSX_Pagination.push(
                <span 
                    key={i} 
                    style={
                        currentPage === i ? 
                            {textDecoration: 'none', color: 'red', fontSize: 22, cursor: 'pointer'}
                        :
                            {textDecoration: 'none', color: 'blue', fontSize: 18, cursor: 'pointer'}
                } onClick={() => setCurrentPage(i)}>{i}&nbsp;&nbsp;</span>
            )
        }

        return JSX_Pagination
    }

    const countryList = generateCountryList(countries ? countries : props.countries)
    const paginationElements = generatePagination(pagination)

    return (
        <div className="CountryList">
            <p>Country name:</p>
            <Search handleSearch={(countryName: string) => props.handleSearch(countryName)} />
            <div className="Divider"></div>
            <ul>
                {countryList}
            </ul>
            <div className="Divider"></div>
            <div className="Pagination"><span style={{float: 'right'}}>Page: {currentPage} of {pagination} | Elements per page: 20</span>{paginationElements}</div>
        </div>
    )
}