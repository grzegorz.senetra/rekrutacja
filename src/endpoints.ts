export const COUNTRIES_BY_NAME_LINK = 'https://restcountries.eu/rest/v2/name'
export const COUNTRY_BY_CODE = 'https://restcountries.eu/rest/v2/alpha'