export type CountryNameAndCode = {
    code: string,
    name: string
}

export type Country = {
    code: string,
    name: string
    capital: string
    currencies: Array<Currency>
}

export type Currency = {
    code: string
    name: string
    symbol: string
}